#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP(x , y) 			make_pair(x ,y)
#define 	MPP(x , y , z) 	    MP(x, MP(y,z))
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<long long ,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
#define II ({int a; scanf("%d", &a); a;})
#define LL ({ll a; scanf("%lld", &a); a;})

typedef long long ll;

using namespace std;
//
vector <ll>vec;
ll dp[1001][1000];
ll mincost(ll s , ll sum )
{
    if(dp[s][sum] != -1)
        return dp[s][sum];
    if(s == vec.size()-1)
        return sum;
    if(dp[s][sum] == 0)
        return MAXN;
    for(ll i = s + 1 ; i<vec.size() && i <= s + vec[s] ; i++)
    {
        if(dp[s][sum] == -1)
            dp[s][sum] = mincost(i , sum+1);
        else
            dp[s][sum] = min(dp[s][sum] , mincost(i , sum+1));
    }
    return dp[s][sum];
}
int main()
{
    ll t;
    //Test;
    cin>>t;
    while(t--)
    {
        ll n ;
        cin>>n;
        Set(dp , -1);
        Rep(i ,n)
        {
            ll x;
            cin>>x;
            vec.push_back(x);
        }
        cout << mincost(0 , 0) <<endl;
        vec.clear();
    }
    return 0;
}
